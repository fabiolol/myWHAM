# myWHAM

A python version of the non-parametric variant weighted histogram analysis Method (WHAM) implemented by Enkavi at al.

```
@article{Enkavi2017,
author = {Enkavi, Giray and Mikkolainen, Heikki and G{\"{u}}ng{\"{o}}r, Bur{\c{c}}in and Ikonen, Elina and Vattulainen, Ilpo},
doi = {10.1371/journal.pcbi.1005831},
editor = {MacKerell, Alexander},
issn = {1553-7358},
journal = {PLOS Computational Biology},
month = {oct},
number = {10},
pages = {e1005831},
publisher = {Public Library of Science},
title = {{Concerted regulation of npc2 binding to endosomal/lysosomal membranes by bis(monoacylglycero)phosphate and sphingomyelin}},
url = {http://dx.plos.org/10.1371/journal.pcbi.1005831},
volume = {13},
year = {2017}
}
```
