import sys
import glob
import re
import numpy as np
import pandas as pd


# Function for sorting files numerically with 1 digits

def numericalSort(value):
    numbers = re.compile(r'(\d+)')
    parts = numbers.split(value)
    parts[1::2] = map(int, parts[1::2])
    return parts


def parameterCatcher(folder, inputFiles, toCatch):
    param = []
    for file in sorted(glob.glob(folder + inputFiles + ".*"), key=numericalSort):
        [param.append(float(re.search(toCatch,line).group(1))) 
        for line in open(file, "r").readlines() if re.search(toCatch,line)]
        print (file)
    return np.array(param)

def calcPotEnergy_1D(outputFile, folder, inputFiles, 
                    KAPPA, AT, initT, finalT, step):
    # The potential Energy is calculating by 0.5* KAPPA *(colvar - AT)**2
    colvar = []
    U      = []

    with open(outputFile,"w") as fout:
        for idx,file in enumerate(sorted(glob.glob(folder + inputFiles + ".*"), key=numericalSort)):
            # The first coloumn is used as simulation time. If floats they are converted in integers. 
            DATA = np.loadtxt(file,comments=["#","@"])
            # Removing duplicates  considering the 1st column (Time) but keeping
            # the latest entries.
            DATA = pd.DataFrame(data=DATA)
            DATA = np.array(DATA.drop_duplicates(subset=0,keep="last"))
            
            time = DATA[:,0]
            z = DATA[:,1]
            colvar.append (z[initT:finalT+1:step])
            #bias = DATA[:,2]
            u = []
            print(file)
            for i in (time[initT:finalT+1:step]):
                a = []
                for j in range(len(KAPPA)):
                    a.append(0.5*KAPPA[j]*(z[int(i)]-AT[j])**2)
                u.append(np.c_[idx,i,np.array(a).reshape(1,len(a))])
            for i in u :
                U.append(i)
                np.array(i[0][:2]).tofile(fout,sep=" ",format="%i ")
                np.array(i[0][2:]).tofile(fout,sep=" ",format="%f")
                fout.write("\n")
                
    colvar = np.concatenate(colvar,axis=0)
    U = np.around( np.concatenate(U,axis=0),decimals=6) 
    np.savetxt("colvar_related_to_" + outputFile, colvar , fmt="%0.6f")
    return U, colvar

def histo_1D(Bootstrap, colvar, Units, Temperature, weights, N_WINDOWS, RANGE, bootstrap_alignment ):
    
    if Units == "kCal":
        kBT= 0.0019872041 * Temperature
    
    elif Units == "kJoul":
        kBT= 0.0083144621 * Temperature
    
    elif Units == "kT":
        kBT= 1   

    if Bootstrap > 0:
        len_chunk = len(weights)//(Bootstrap+1)
        profile = []
        for i in range (0,Bootstrap+1):
            hist = ( np.histogram(colvar, bins=N_WINDOWS-1, range=RANGE, 
                         normed=False, weights=weights[len_chunk*i:len_chunk*(i+1)], density=False)[0] )
        
            y   =  -kBT*np.log(hist)

            if bootstrap_alignment == "min":
                    y -= np.min(y)
            elif bootstrap_alignment == "max":
                for i in hist:
                    y -= np.max(y)
            elif bootstrap_alignment == "first":
                for i in hist:
                    y -= y[0]
            elif bootstrap_alignment == "last":
                for i in hist:
                    y -= y[-1]
            elif bootstrap_alignment == "no":
                pass

            profile.append(y)


        y    =  np.mean(profile,axis=0)
        yerr =  np.std (profile,axis=0)
        return y, yerr
    
    else:
        hist = np.histogram(colvar, bins=N_WINDOWS-1, range=RANGE, 
                         normed=False, weights=weights, density=False)
        
        y = -kBT*np.log( hist[0])
        yerr = 0
        return y, yerr


def WHAM(Bias_File, Pm, Units, Temperature, Tolerance, Bootstrap, out_File, log_File):
    print ("Inputs Given:")
    print ("Initial Probability:", Pm)
    print ("Units:", Units)
    print ("Temperature:", str(Temperature)+"K")
    print ("Tolerance:",Tolerance)
    print ("Bootstrap", Bootstrap)
    print ("Output File:", out_File)
    print ("Log File:", log_File)


    # Import the data from a list and/or array
    data = np.array(Bias_File)

    # Opening the output files
    of   = open(out_File, 'w')
    lf   = open(log_File, 'w')


    
    print ("# wham.py", Bias_File, Pm, Units, Temperature, Tolerance, Bootstrap, out_File, log_File, file=lf)
    
    
    col  = 0
    wn   = data[:,col]
    Nw   = np.unique(wn).size
    print ("# Number of windows: ", Nw, file=lf)

    col += 1
    
    t     = data[:,col]
    Nconf = t.size
    print ("# Total number of configurations: ", Nconf, file=lf)
    
    col  +=1
    
    T = np.float(Temperature)
    if (Units=='kJoul'):

        # Boltzmann constant in kJ/(mol.K)
        kB=0.0083144621
        beta=1.0/(kB*T)
    elif (Units=='kCal'):
        # Boltzmann constant in kcal/(mol.K)
        kB=0.0019872041
        beta=1.0/(kB*T)
    elif (Units=='kT'):
        beta=1.0
    else:
        print ("# Units has to be kJoul, kCal, or kT")
        sys.exit(2)
    
    print ("# Thermodynamic beta = ", beta, file=lf)
    
    # Bias function exponential
    bf = np.exp(-beta*data[:,col:Nw+col])
    col += Nw
    
    if ( Pm == 0):
        pm=np.ones(Nconf)
        print ("# No initial configuration weights will be taken into account.", file=lf)
    elif ( Pm ==  1):
        pm = data[:,col]
        print ("# Initial configuration weights are taken from column= ", col, file=lf)
        col += 1
    elif (Pm == 'ln'):
        pm = np.exp(beta*data[:,col])
        print ( "# Initial configuration weights are taken and exp(beta*column), column= ", col , file=lf)
        col+=1
    else:
        print ("Pm has to be 0, 1, or ln")
        sys.exit(2)
    
    Nboots=np.int(Bootstrap)
    if (Nboots>0):
        print ("# Number of bootstrap itereations= ", Nboots, file=lf)
        if (col >= data.shape[1]):
            blocks=data[:,0]
            print ("# Each simulation will be used as a single block.", file=lf)
        else:
            blocks = data[:,col]
            print ("# Blocks are assigned based on column= ", col, file=lf)
        Nblocks=np.unique(blocks).size
        print ("# Total number of blocks= ", Nblocks, file=lf)
    
    pboots = np.ones(Nconf)
    
    #inititalize c's
    cini = np.ones(Nw)
    
    #iterate until tolerance (in kT)
    tol = np.float(Tolerance)
    
    results = [[],[],[],[]]
    # bootstrapping
    for bs in range(0,Nboots+1):
        print ("# ", bs, " Iterations lndc", file=lf)
        print ("# ", bs, " Iterations")
        # in bs 0 we do not do any bootstrap but regular wham
        #
        # total prior weights (e.g., weigths from metadynamics)
        # N[i] is equal to number of samples in window i when there are no prior weights
        pp = pboots*pm
    
        N = np.empty(Nw)
        for i in range(0,Nw):
           wni=np.unique(wn)[i]
           N[i]=sum(pp[wn==wni])
        #
        c = cini
        it = 0
        lndc = 10**100
        while lndc >= tol:
            #save the previous values to check convergence
            cpre = c
            # The following sum is over all columns (i)
            p = pp/np.sum(N*c*bf,axis=1)
    
            # The following sum is over all rows (t)
            c = 1.0/np.sum(p[np.newaxis].T*bf,axis=0)
    
            lndc=np.log(np.max(c/cpre))
            print (bs,it,lndc , file=lf)
            it += 1
        print ("# ", bs, " Final estimate of c", file=lf)

        print (c, file=lf)
        #
        # finalize p -- calculate it again once the convergence is reached
        p  = pp/np.sum(N*c*bf,axis=1)
        p /= np.sum(p)
        #
        if bs == 0:
            cini = c
        
        
        for i in range(0,Nconf):
            print (bs, int(wn[i]), t[i], p[i], file=of)
            results[0].append(int(bs))
            results[1].append(int(wn[i]))
            results[2].append(int(t[i])) 
            results[3].append(p[i])

        if (Nboots>0):
            #bayesian weights
            u = np.empty(Nblocks+1)
            u[0] = 0.0
            u[-1] = 1.0
            u[1:-1] = np.sort(np.random.uniform(0,1,Nblocks-1))
            g = u[1:]-u[:-1]
            pboots = np.zeros(Nconf)
            for i in range(0,Nblocks):
                blocki = np.unique(blocks)[i]
                pboots[blocks == blocki] = g[i]/blocks[blocks == blocki].size

    results = np.array(results)
    print ("Done.")
    return np.c_[results[0],results[1],results[2],results[3]]
